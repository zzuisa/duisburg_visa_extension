import traceback
from email.mime.text import MIMEText
from email.header import Header
import email.utils
import smtplib


def mail(to_addr: object, content: object) -> bool:
    try:
        html_content = """
        <html>
        <head>
        <style>
        body {{
            font-family: Arial, sans-serif;
            background-color: #F2F2F2;
        }}
        h1 {{
            color: #333333;
            text-align: center;
            margin-top: 40px;
        }}
        p {{
            font-size: 16px;
            line-height: 24px;
            color: #666666;
            margin: 0 0 20px;
        }}

        .button {{
            display: inline-block;
            margin: 20px 0;
            padding: 10px 20px;
            border-radius: 5px;
            background-color: #007FFF;
            color: #FFFFFF !important;
            font-weight: bold;
            text-decoration: none;
            text-align: center;
            cursor: pointer;
        }}

        .button:hover {{
            background-color: #0066CC;
        }}
        </style>
        </head>
        <body>
            <h1>Welcome to our Duisburg Helper Center!</h1>
            <p>Dear subscriber,</p>
            <p>We are excited to share with you the latest news and updates from our company. Please find below a summary of our recent activities:</p>
            <ul>
                <li>杜伊斯堡外管局南局官网延签最新时间：</li>
                <li>{}</li>
            </ul>
            <p>Don't miss out on these exciting developments! To learn more, click the button below:</p>
            <a href="https://www.qtermin.de/qtermin-stadt-duisburg-abh-sued" class="button">Learn more</a>
        </body>
        </html>
        """.format(content)
        from_format = email.utils.formataddr(('DuisburgHelper', 'roguelife2023@gmail.com'))
        from_addr = 'roguelife2023@gmail.com'
        password = 'qtfvjxhgtrbsxhue'
        smtp_server = 'smtp.gmail.com'
        msg = MIMEText(html_content, 'html', 'utf-8')
        msg['From'] = from_format
        msg['To'] = Header(to_addr)
        msg['Subject'] = Header('Duisburg南局延签信息，请检查。')
        server = smtplib.SMTP_SSL(smtp_server)
        server.connect(smtp_server, 465)
        server.login(from_addr, password)
        server.sendmail(from_addr, to_addr, msg.as_string())
        server.quit()
        return True
    except Exception as e:
        traceback.print_exc()
        return False
