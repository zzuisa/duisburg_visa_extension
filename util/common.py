import urllib.parse
import wave
import pyaudio
import time


def play_audio(filename='8069.wav'):
    chunk = 1024
    wf = wave.open(f'static/media/{filename}', 'rb')
    p = pyaudio.PyAudio()
    stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
                    channels=wf.getnchannels(),
                    rate=wf.getframerate(),
                    output=True)
    data = wf.readframes(chunk)
    while len(data) > 0:
        stream.write(data)
        data = wf.readframes(chunk)
    stream.stop_stream()
    stream.close()
    p.terminate()


def encode(_str):
    return urllib.parse.quote(_str)


def print_and_record(txt, file_name='system'):
    cur_time_str = time.strftime('%Y-%m-%dT%H:%M:%S', time.localtime())
    f = open(f"logs/logs-{file_name}", 'a', encoding='utf-8')
    if not file_name == 'system':
        fs = open(f"logs/logs-system", 'a', encoding='utf-8')
        fs.write(f'[{cur_time_str}] - {str(txt)}' + '\n')
    f.write(f'[{cur_time_str}] - {str(txt)}' + '\n')
    print(f'[{cur_time_str}] - {txt}')
