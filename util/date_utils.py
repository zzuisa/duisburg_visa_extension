import datetime
from datetime import datetime as dt, timedelta
from dateutil.relativedelta import relativedelta
import random


def last_day_of_month(any_day):
    next_month = any_day.replace(day=28) + datetime.timedelta(days=4)  # this will never fail
    return next_month - datetime.timedelta(days=next_month.day)


def get_between_day(begin_date, ext_month=1):
    date_list = []
    begin_date = datetime.datetime.strptime(begin_date, "%Y-%m-%d")
    end_date = last_day_of_month(
        datetime.datetime(datetime.datetime.now().year, datetime.datetime.now().month + ext_month, 1))
    while begin_date <= end_date:
        date_str = begin_date.strftime("%Y-%m-%d")
        date_list.append(date_str)
        begin_date += datetime.timedelta(days=1)
    return date_list


def get_available_time(today=dt.now().date()):
    # 获取当前日期
    # 获取下个月的第一天
    today = dt.strptime(today, "%Y-%m-%d")
    next_month = (today.replace(day=1) + relativedelta(months=1))
    # 创建日期列表
    # date_list = [today.strftime('%Y-%m-%d'), next_month.strftime('%Y-%m-%d')]
    date_list = [today.strftime('%Y-%m-%d')]

    return date_list


def date_format(date_str):
    return dt.strptime(date_str, '%Y-%m-%dT%H:%M:%S').strftime('%Y-%m-%d %H:%M')


def parse_str(s):
    start, end = s.split('@')
    start_date = dt.strptime(start, '%Y-%m-%dT%H:%M:%S')
    end_date = dt.strptime(end, '%Y-%m-%dT%H:%M:%S')
    return start_date, end_date

def specify_date(time_list,target_date):
    # target_date 是个list 存储了预期的预约时间
    filtered_times = []
    for time_range in time_list:
        start_time, end_time = time_range.split("@")
        start_datetime = datetime.datetime.fromisoformat(start_time)
        end_datetime = datetime.datetime.fromisoformat(end_time)

        for t_date in target_date:
            if start_datetime.date() == datetime.date.fromisoformat(
                    t_date) or end_datetime.date() == datetime.date.fromisoformat(t_date):
                filtered_times.append(time_range)
    return filtered_times

def date_range(start_date, end_date):
    """生成指定日期范围内的日期列表"""
    dates = []
    current_date = start_date
    while current_date <= end_date:
        dates.append(current_date.strftime("%Y-%m-%d"))
        current_date += timedelta(days=1)
    return dates


def lookup_best_time(time_list):
    # 将字符串解析为 datetime 对象的列表，并且与字符串一起排序
    time_list = specify_date(time_list, date_range('2024-04-04', '2024-04-13'))
    # time_list = specify_date(time_list, ['2023-07-14','2023-07-15','2023-07-16','2023-07-17','2023-07-18','2023-07-19','2023-07-20','2023-07-21'])
    if len(time_list) == 0:
        return ''
    else:
        dates_strs_sorted = sorted([(parse_str(s), s) for s in time_list], key=lambda x: (x[0][0].date(), x[0][0].time()))
        # 从后向前取，直到取够10条记录
        latest_earliest_date_strs = [s for _, s in dates_strs_sorted[-10:]]
        target = latest_earliest_date_strs[random.randint(0, len(latest_earliest_date_strs) - 1)]
        return target.split('@')[0], target.split('@')[1]
