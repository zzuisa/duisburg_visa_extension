import datetime
import time
import traceback
import os
import random
from datetime import datetime as dt
from duisburg_visa_extension.util.mail import mail
from duisburg_visa_extension.util.common import print_and_record, play_audio
from duisburg_visa_extension.util.date_utils import get_available_time, lookup_best_time, get_between_day
from duisburg_visa_extension.service.v1.scan_and_send import doSend, doscan
import threading

mail_lock = threading.Lock()


def main_program(candidate):
    params = candidate[0]
    service = candidate[1]
    mailed = False
    retry = 0
    while True:
        current_time = dt.now().time()
        if not (datetime.time(7, 0) <= current_time <= datetime.time(23, 59)):
            pass
            # 输出不可用时间段提示信息
            # print_and_record(f'{params[1]}-{service}:当前为休息时间(22:00 - 10:00)，暂停服务', params[1])
            # time.sleep(2070)  # 等待2070秒后重新检查可用时间段
            # continue
        time.sleep(random.randint(1, 5) * 0)
        print_and_record(f'------------------------{params[1]}-{service} start-----------------------------', params[1])
        if retry > 5:
            break
        try:
            custom_date = '2024-01-01'
            start_date = time.strftime('%Y-%m-%d', time.localtime(time.time()))
            safe_date = get_available_time(custom_date)
            date_range = get_between_day(custom_date, 2)
            print_and_record(f'{params[1]}:Parent process {os.getpid()}', params[1])
            cal_item, data_collector = doscan(params[1], safe_date, service)
            print_and_record(f'{params[1]}:--当前所有批次执行完毕--', params[1])
            print_and_record(f'{params[1]}:{data_collector}', params[1])
            print_and_record(f'{params[1]}:过滤后剩余%s' % len(data_collector), params[1])

            if len(data_collector) != 0:
                target_date_and_time = lookup_best_time(data_collector)
                if len(target_date_and_time) != 0:
                    play_audio('mail.wav')
                    if not mailed:
                        with mail_lock:
                            mailed = mail('zzuisa.cn@gmail.com',
                                          f'{params[1]}-{service}:最佳结果：时间段：[{target_date_and_time[0]}-{target_date_and_time[1]}]')
                    print_and_record(f'{params[1]}:最佳结果：时间段：[{target_date_and_time[0]}-{target_date_and_time[1]}]',
                                     params[1])
                    calendar_item = cal_item[f"{target_date_and_time[0]}@{target_date_and_time[1]}"]
                    res = doSend(*params, target_date_and_time[0], target_date_and_time[1], service, calendar_item)
                    retry += 1
                    if res:
                        print_and_record(f'------------------------{params[1]}-{service} end-----------------------------',params[1])
                        break
            print_and_record(f'------------------------{params[1]}-{service} end-----------------------------', params[1])
            time.sleep(random.randint(1, 5) * 0)
        except Exception as e:
            traceback.print_exc()
            print_and_record(f'---------------------------{params[1]} end--------------------------------', params[1])
