import time
from datetime import datetime as dt
import requests
import random
import traceback
from duisburg_visa_extension.util.common import encode, play_audio, print_and_record
from duisburg_visa_extension.util.date_utils import last_day_of_month
from json import loads
from duisburg_visa_extension.config.service import services
from duisburg_visa_extension.config.common import *


def doSend(anrede, first_name, last_name, address,zip, e_mail, birthday, telephone, start, end, service, calendar_item):
    i_start = start.split('T')
    i_end = end.split('T')
    start = '%s %s' % (i_start[0], i_start[1][0:5])
    end = '%s %s' % (i_end[0], i_end[1][0:5])
    appgroup = f'2b01fc04-{str(random.random())[2:6]}-42fa-a6b7-5e28598ce7d4'
    # appgroup = '2b01fc04-6874-42fa-a6b7-5e28598ce7d4'
    bookerinfo = f"""Anrede\t{anrede}\r\nVorname\t{first_name}\r\nName\t{last_name}\r\nStrasse\t{address}\r\nPLZ\t{zip}\r\nOrt\tDuisburg\r\nTelefon\t{telephone}\r\nE-Mail\t{e_mail}\r\nGeburtsdatum\t{birthday}\r\n\t\r\nTerminerinnerung\t12 Stunden vor Termin\r\n"""
    data = f'language=de&bookingtype=Internet&bookingurl=https%3A%2F%2Fwww.qtermin.de%2Fqtermin-stadt-duisburg-abh-sued&agbaccepted=false&dataprivacyaccepted=true&feedbackpermissionaccepted=false' \
           f'&newsletter=false&services={services[service][0]}' \
           f'&FirstName={encode(first_name)}&LastName={encode(last_name)}&Email={encode(e_mail)}&Birthday={encode(birthday)}&Street={encode(address)}&ZIP={zip}&City=Duisburg&Phone={encode(telephone)}&Salutation={anrede}&Notes=' \
           f'&bookerinfo={encode(bookerinfo)}&calendarname={encode(calendar_item["calname"])}&start={start}&end={end}&calendarid={calendar_item["calendarid"]}&calname={encode(calendar_item["calname"])}&location=Sittardsberger%20Allee%2014%2C%2047249%20Duisburg&tzaccount=W. Europe Standard Time&checkexist=1' \
           f'&pricegross=0&appgroup={encode(appgroup)}&capacity=1&servicescapacity={{"{services[service][0]}":"1"}}' \
           f'&canceldeadline=0&sync=1&sendemail=1&appointmentreminderhours=-12&appointmentreminderhours2=-4&confirmappointment=1&confirmtime=120&sendinvoice=1&nrappbooked=1&capused=false&capmaxused=30&customerconfirm=true&calselid=-1&lnm=1&emailm=1&storeip=false&apw=false'

    response = requests.post('https://www.qtermin.de/api/appointment', headers=headers, cookies=cookies, data=data)
    print_and_record(f'{first_name}:{response.text}', first_name)
    res = None
    try:
        res = loads(response.text)
    except Exception as e:
        return False
    if res['Status'] == 200 or res['Status'] == '200':
        f = open("data/data.txt", 'a')
        f.write(str(res))
        play_audio()
        print_and_record(f'{first_name}:在2小时内检查邮箱并验证！', first_name)
        return True


def doscan(name, safe_date, service):
    try:
        q = []
        start = time.time()
        params['serviceid'] = services[service][0]
        params['duration'] = services[service][1]
        params_for_times['serviceid'] = services[service][0]
        params_for_times['duration'] = services[service][1]

        cal_item = {}
        for _d in safe_date:
            params['date'] = _d
            print_and_record(
                f'{name}:正在扫描当前日期:[{_d} -> {last_day_of_month(dt.strptime(_d, "%Y-%m-%d")).strftime("%Y-%m-%d")}]',
                name)
            time.sleep(1)
            response = requests.get('https://www.qtermin.de/api/timeslots', params=params, cookies=cookies,
                                    headers=headers)
            target = loads(response.text)
            for index, i in enumerate(target):
                if i['calendarid'] != '' and i['calendarid'] != None:
                    print_and_record('当天结果{}-{}'.format(i['start'], i['end']),name)
                    params_for_times['date'] = i['start'].split('T')[0]
                    time_response = requests.get('https://www.qtermin.de/api/timeslots', params=params_for_times,
                                                 cookies=cookies, headers=headers_for_times)
                    time_target = loads(time_response.text)
                    for _index, _i in enumerate(time_target):
                        combined_time = f"{_i['start']}@{_i['end']}"
                        cal_item[combined_time] = {
                            'calendarid': _i['calendarid'],
                            'calname': _i['calname'] if 'calname' in dict(_i).keys() else _i['calendarname']
                        }
                        q.append(combined_time)
                        print_and_record('{}-{}'.format(_i['start'].split('T')[1], _i['end'].split('T')[1]),name)
                        # if i['start'] > '10:00' \
                        #         and i['end'] < '18:00' \
                        #         and i['calendarname'] in ['Bürger-Service-Station-Mitte', 'Bürger-Service-Station-Süd']:
                        #     q.put(i)
                        # data_collector.append(i)
                        # if safe_date[-1] == _d and index == len(target)-1 and q.qsize()==0:
                        #     for item in data_collector:
                        #       # if item['calendarname'] == 'Bürger-Service-Station-Süd' :
                        #         item_start = item['start'].split('T')[1].split(":")
                        #         if(math.fabs(int(item_start[0])*60 + int(item_start[1]) - 720)<120):
                        #             q.put(item)
                        #     print('共命中%s条结果' % len(data_collector))
        end = time.time()
        print_and_record(f'{name}:Task runs %0.2f seconds.' % (end - start), name)
        return cal_item, q

    except Exception as e:
        traceback.print_exc()
