import concurrent.futures
from service.main_program import main_program
from config.candidates import *
if __name__ == '__main__':

    # 创建线程池
    # thread_pool = concurrent.futures.ThreadPoolExecutor()
    # 创建进程池
    thread_pool = concurrent.futures.ProcessPoolExecutor()
    # 提交任务到线程池，并传递参数
    future = thread_pool.submit(main_program, sx)
    # dy_future = thread_pool.submit(main_program, dy)
    # xy_future = thread_pool.submit(main_program, xy)
    # test_future = thread_pool.submit(main_program, test)
    # 提交更多任务...
    # thread_pool.submit(main_program, param1_value2, param2_value2, param3_value2)
    concurrent.futures.wait([future])
    # concurrent.futures.wait([sa_future,test_future])
    # concurrent.futures.wait([dy_future])
    thread_pool.shutdown(wait=True)
